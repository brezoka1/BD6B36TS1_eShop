/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.eshop.main;

import cz.cvut.eshop.archive.PurchasesArchive;
import cz.cvut.eshop.shop.DiscountedItem;
import cz.cvut.eshop.shop.EShopController;
import static cz.cvut.eshop.shop.EShopController.purchaseShoppingCart;
import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.ShoppingCart;
import cz.cvut.eshop.shop.StandardItem;
import cz.cvut.eshop.storage.NoItemInStorage;
import cz.cvut.eshop.storage.Storage;

/**
 *
 * @author Karel
 */
public class Shop {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws NoItemInStorage {
        EShopController.startEShop();
        
        /* make up an artificial data */
        
        int[] itemCount = {10,10,4,5,10,2};
               
        
        Item[] storageItems = {
            new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
            new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
            new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
            new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
            new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
            new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
        };
        
        Storage storage = EShopController.getStorage();
        PurchasesArchive archive = EShopController.getArchive();
        
        // insert data to the storage
        for (int i = 0; i < storageItems.length; i++) {
            storage.insertItems(storageItems[i], itemCount[i]);
        }
        
        
        storage.printListOfStoredItems();
        
        System.out.println();
        
        System.out.println("TEST RUN:   Fill and purchase a shopping cart");
        ShoppingCart newCart = new ShoppingCart();
        newCart.addItem(storageItems[0]);
        newCart.addItem(storageItems[1]);
        newCart.addItem(storageItems[2]);
        newCart.addItem(storageItems[4]);
        newCart.addItem(storageItems[5]);
        purchaseShoppingCart(newCart, "Libuse Novakova","Kosmonautu 25, Praha 8");
        archive.printItemPurchaseStatistics();
        storage.printListOfStoredItems();
        
        
        System.out.println();
        
                
        System.out.println("TEST RUN:    Trying to purchase an empty shopping cart");
        
        ShoppingCart newEmptyCart = new ShoppingCart();
        purchaseShoppingCart(newEmptyCart, "Jarmila Novakova", "Spojovaci 23, Praha 3");
        
        
        
    }

}
